package poloczek.grzegorz.pl.carsquiz;


import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import poloczek.grzegorz.pl.carsquiz.data.QuizDbQueries;
import poloczek.grzegorz.pl.carsquiz.presenter.QuizFinishPresenter;
import poloczek.grzegorz.pl.carsquiz.utils.DialogHelper;
import poloczek.grzegorz.pl.carsquiz.view.QuizFinishView;


public class QuizFinish extends AppCompatActivity implements QuizFinishView {
    private int score;
    private int totalQuestion;
    private int receivedScore;
    private int receivedQuestions;

    @BindView(R.id.quizUserScore)
    TextView scorePoints;

    private QuizFinishPresenter presenter;
    private int countSave = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_finish);
        ButterKnife.bind(this);
        presenter = new QuizFinishPresenter(this);
        presenter.loadDbQueries();
        receivedScore = getIntent().getExtras().getInt("SCORE",score);
        receivedQuestions = getIntent().getExtras().getInt("TOTAL", totalQuestion);

        presenter.playSound(receivedScore, receivedQuestions);
    }

    @OnClick(R.id.showScoreboard) public void scoreBoard(){
        presenter.showScoreBoard();
    }

    @OnClick(R.id.saveScore)
    public void saveScore() {
        if (countSave == 0) {
            presenter.saveScoreDialog();
        } else {
            presenter.showToast();
        }
    }

    @Override
    public void showScoreBoard() {
        Intent intent = new Intent(this, QuizScoreboardActivity.class);
        startActivity(intent);
    }

    @Override
    public void saveScoreDialog() {
        DialogHelper.saveScoredDialog(this, R.string.quiz_save_score_alert,
                R.string.quiz_scoreboard_nickname, R.string.cancel, R.string.quiz_save_score_alert_positive,
                receivedScore, presenter);
        countSave++;
    }

    @Override
    public void showToast() {
        Toast.makeText(this, R.string.quiz_save_score_already, Toast.LENGTH_LONG).show();
    }

    @Override
    public MediaPlayer musicPlayerInit(int musicPath) {
        return MediaPlayer.create(this, musicPath);
    }

    @Override
    public QuizDbQueries initializeQueriesView() {
        return new QuizDbQueries(this);
    }

    @Override
    public void setScorePoints() {
        scorePoints.setText(String.valueOf(receivedScore));
    }

}
