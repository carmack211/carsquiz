package poloczek.grzegorz.pl.carsquiz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import java.util.List;
import poloczek.grzegorz.pl.carsquiz.data.QuizDbQueries;
import poloczek.grzegorz.pl.carsquiz.data.QuizRankingModel;

public class QuizScoreboardActivity extends AppCompatActivity {

    private RecyclerView scoreRecyclerView;
    private QuizScoreboardAdapter adapter;
    private QuizDbQueries db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_scoreboard);
        scoreRecyclerView = (RecyclerView) findViewById(R.id.scoreRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        scoreRecyclerView.setLayoutManager(linearLayoutManager);

        db = new QuizDbQueries(this);
        List<QuizRankingModel> lstRanking = db.getRanking();

        if(lstRanking.size() > 0)
        {
            adapter = new QuizScoreboardAdapter(getApplicationContext(),lstRanking);
            scoreRecyclerView.setAdapter(adapter);
        }
    }

    @Override
    protected void onDestroy() {
        db.close();
        super.onDestroy();
    }
}
