package poloczek.grzegorz.pl.carsquiz.data;

import android.provider.BaseColumns;

/**
 * Created by RENT on 2017-08-01.
 */

public class QuizContract {

    private QuizContract() {
    }

    public static class QuizEntry implements BaseColumns {
        public static final String TABLE_NAME = "Quiz";
        public static final String COLUMN_IMAGE = "Image";
        public static final String COLUMN_ANSWERA = "AnswerA";
        public static final String COLUMN_ANSWERB = "AnswerB";
        public static final String COLUMN_ANSWERC = "AnswerC";
        public static final String COLUMN_ANSWERD = "AnswerD";
        public static final String COLUMN_CORRECT_ANSWER = "CorrectAnswer";
    }

    public static class RankingEntry implements BaseColumns {
        public static final String TABLE_NAME = "Ranking";
        public static final String COLUMN_NAME = "Name";
        public static final String COLUMN_SCORE = "Score";

    }



}
