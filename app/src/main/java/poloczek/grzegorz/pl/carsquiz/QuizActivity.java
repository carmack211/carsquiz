package poloczek.grzegorz.pl.carsquiz;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;;
import java.util.List;
import java.util.Random;
import poloczek.grzegorz.pl.carsquiz.data.QuizDbQueries;
import poloczek.grzegorz.pl.carsquiz.data.QuizQuestionsModel;

public class QuizActivity extends AppCompatActivity {
    final static long INTERVAL = 1000; // 1 second
    final static long TIMEOUT = 10000; // 10 sconds
    private QuizDbQueries dbQueries;
    private List<QuizQuestionsModel> quizQuestionsList;
    private Random randomQuestion = new Random();
    private CountDownTimer countDownTimer;
    private ProgressBar progressBar;
    private ImageView imageView;
    private Button btnA, btnB, btnC, btnD;
    private Button clickedButton;
    private TextView txtScore, txtQuestion;
    private TextView multiplerText;
    private int index = 0, score = 0, thisQuestion = 0, progressValue = 0, totalQuestion, correctAnswer;
    private int questions;
    private Animation shakeAnimation;
    private float multipler=1.0f;
    private boolean onBackPressed = false;
    private MediaPlayer player;
    private int questionsCounter = 15, counter = 15;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        txtScore = (TextView) findViewById(R.id.scorePoints);
        txtQuestion = (TextView) findViewById(R.id.progressNumber);
        multiplerText = (TextView) findViewById(R.id.multipler);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        imageView = (ImageView) findViewById(R.id.question_image);
        btnA = (Button) findViewById(R.id.btnAnswerA);
        btnB = (Button) findViewById(R.id.btnAnswerB);
        btnC = (Button) findViewById(R.id.btnAnswerC);
        btnD = (Button) findViewById(R.id.btnAnswerD);

        multiplerText.setText(String.valueOf(multipler));

        dbQueries = new QuizDbQueries(QuizActivity.this);
        quizQuestionsList = dbQueries.initQuestions();

        questions = quizQuestionsList.size();

        shakeAnimation = AnimationUtils.loadAnimation(this,
                R.anim.incorrect_shake);
        shakeAnimation.setRepeatCount(3);

        countDownTimer = new CountDownTimer(TIMEOUT, INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                progressValue++;
                progressBar.setProgress(progressValue);
                Log.d("D", String.valueOf(progressValue));
            }

            @Override
            public void onFinish() {
                progressValue++;
                progressBar.setProgress(progressValue);
                Log.d("DI", String.valueOf(progressValue));
                changeColor();
               if(progressValue == 10) {
                   resetMultipler();
                }
                quizQuestionsList.remove(index);
                showQuestion();
            }
        };
        showQuestion();
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        onBackPressed = true;
        this.finish();
    }

    private void showQuestion() {
        totalQuestion = questionsCounter;

        if (onBackPressed == false) {
            questionsCounter --;
            if (totalQuestion != 0) {
                if (questions == 1) {
                    index = 0;
                } else {
                    index = randomQuestion.nextInt(questions - 1);
                }
                changeColor();
                thisQuestion++;
                questions--;
                txtQuestion.setText(String.format("%d/%d", thisQuestion, counter));
                progressBar.setProgress(0);
                progressValue = 0;
                int ImageId = this.getResources().getIdentifier(quizQuestionsList.get(index).getImage().toLowerCase(), "drawable", getPackageName());
                imageView.setBackgroundResource(ImageId);
                btnA.setText(quizQuestionsList.get(index).getAnswerA());
                btnB.setText(quizQuestionsList.get(index).getAnswerB());
                btnC.setText(quizQuestionsList.get(index).getAnswerC());
                btnD.setText(quizQuestionsList.get(index).getAnswerD());
                countDownTimer.start();
            } else {
                Intent intent = new Intent(this, QuizFinish.class);
                intent.putExtra("SCORE", score);
                intent.putExtra("TOTAL", questions * 10);
                //intent.putExtra("CORRECT", correctAnswer);
                startActivity(intent);
                finish();
            }
        }
    }

    private void changeColor() {
        btnA.setBackgroundColor(getResources().getColor(R.color.buttonQuiz));
        btnB.setBackgroundColor(getResources().getColor(R.color.buttonQuiz));
        btnC.setBackgroundColor(getResources().getColor(R.color.buttonQuiz));
        btnD.setBackgroundColor(getResources().getColor(R.color.buttonQuiz));
    }

    private void resetMultipler() {
        multipler=1.0f;
        correctAnswer = 0;
        multiplerText.setText(String.valueOf(multipler));
    }

    public void chooseAnswer(View view) {
        clickedButton = (Button) view;
            if (clickedButton.getText().equals(quizQuestionsList.get(index).getCorrectAnswer())) {
                new ChooseAnswerAsync(true).execute();
            } else {
                new ChooseAnswerAsync(false).execute();
            }
    }

    private void setBackgroundColorGreen() {
        clickedButton.setBackgroundColor(Color.GREEN);
    }

    private void setBackgroundColorRed() {
        clickedButton.setBackgroundColor(Color.RED);
    }

    class ChooseAnswerAsync extends AsyncTask<Void, Void, Void> {
        private Boolean passingValue;

        public ChooseAnswerAsync(Boolean passingValue) {
            this.passingValue = passingValue;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (passingValue == true) {
                score += 10*multipler; // increase score
                correctAnswer++; //increase correct answer
                player = MediaPlayer.create(getApplicationContext(), R.raw.good);
                player.start();
                setBackgroundColorGreen();
                if(correctAnswer>2){
                    multipler=1.5f;
                    multiplerText.setText(String.valueOf(multipler));
                }
                if (correctAnswer>4){
                    multipler=2.0f;
                    multiplerText.setText(String.valueOf(multipler));
                }
            } else {
                setBackgroundColorRed();
                imageView.startAnimation(shakeAnimation);
                player = MediaPlayer.create(getApplicationContext(), R.raw.error);
                player.start();
                resetMultipler();
                imageView.animate().withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            quizQuestionsList.remove(index);
            countDownTimer.cancel();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            txtScore.setText(String.format("%d", score));
            showQuestion();
        }
    }

}

