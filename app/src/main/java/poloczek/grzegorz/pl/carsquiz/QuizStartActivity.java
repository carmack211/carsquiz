package poloczek.grzegorz.pl.carsquiz;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

public class QuizStartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_start);
    }

    public void informationAboutQuiz(View view) {

        createAlertDialog(this).show();

    }

    public void startQuiz(View view) {
        Intent intent = new Intent(this, QuizActivity.class);
        startActivity(intent);
    }

    public void showSavedScores(View view) {
        Intent intent = new Intent(this, QuizScoreboardActivity.class);
        startActivity(intent);
    }

    public static AlertDialog createAlertDialog (Context context) {
        final TextView message = new TextView(context);
        final SpannableString s =
                new SpannableString(context.getText(R.string.quiz_informations_msg));
        Linkify.addLinks(s, Linkify.WEB_URLS);
        message.setText(s);
        message.setGravity(Gravity.CENTER_HORIZONTAL);
        message.setPadding(0,30,0,0);
        message.setMovementMethod(LinkMovementMethod.getInstance());

        return new AlertDialog.Builder(context)
                .setTitle(R.string.quiz_informations)
                .setIcon(R.drawable.quiz_logo)
                .setCancelable(false)
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setView(message)
                .create();
    }
}
