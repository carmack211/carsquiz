package poloczek.grzegorz.pl.carsquiz.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;
import android.widget.LinearLayout;
import poloczek.grzegorz.pl.carsquiz.presenter.QuizFinishPresenter;

/**
 * Created by Grzegorz on 2017-12-28.
 */

public class DialogHelper {

    public static void saveScoredDialog(Context context, int title, int hint, int cancelText,
                                        int positiveText, final int receivedScore, final QuizFinishPresenter presenter){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        final EditText newText = new EditText(context);
        LinearLayout layout = new LinearLayout(context.getApplicationContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        newText.setHint(hint);
        layout.addView(newText);
        alertDialog.setView(layout);
        alertDialog.setNegativeButton(cancelText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.dbQueries.addScoreToRankTable(newText.getText().toString(), String.valueOf(receivedScore));
            }
        });
        alertDialog.show();
    }

}
