package poloczek.grzegorz.pl.carsquiz.view;

import android.media.MediaPlayer;
import poloczek.grzegorz.pl.carsquiz.data.QuizDbQueries;

/**
 * Created by Grzegorz on 2017-12-27.
 */

public interface QuizFinishView extends BaseActivityView {

    void showScoreBoard();

    void saveScoreDialog();

    void showToast();

    MediaPlayer musicPlayerInit(int musicPath);

    QuizDbQueries initializeQueriesView();

    void setScorePoints();

}
