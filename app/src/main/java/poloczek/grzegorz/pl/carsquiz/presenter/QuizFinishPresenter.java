package poloczek.grzegorz.pl.carsquiz.presenter;

import android.media.MediaPlayer;
import poloczek.grzegorz.pl.carsquiz.R;
import poloczek.grzegorz.pl.carsquiz.data.QuizDbQueries;
import poloczek.grzegorz.pl.carsquiz.view.QuizFinishView;

/**
 * Created by Grzegorz on 2017-12-27.
 */

public class QuizFinishPresenter {
    private QuizFinishView view;
    private MediaPlayer mediaPlayer;
    public QuizDbQueries dbQueries;

    public QuizFinishPresenter(QuizFinishView view) {
        this.view = view;
    }

    public void showScoreBoard(){
        view.showScoreBoard();
    }

    public void saveScoreDialog(){
        view.saveScoreDialog();
    }

    public void showToast() {
        view.showToast();
    }

    public void playSound(int receivedScore, int receivedQuestions){
        float value = (float) receivedScore / (float) receivedQuestions;
        if (value > 0.75f){
            mediaPlayer = view.musicPlayerInit(R.raw.engine);
            mediaPlayer.start();
        } else if (value >= 0.3f){
            mediaPlayer = view.musicPlayerInit(R.raw.engine_start);
            mediaPlayer.start();
        } else {
            mediaPlayer = view.musicPlayerInit(R.raw.fail);
            mediaPlayer.start();
        }
    }

    public void loadDbQueries(){
        dbQueries = view.initializeQueriesView();
        dbQueries.load();
    }
}
