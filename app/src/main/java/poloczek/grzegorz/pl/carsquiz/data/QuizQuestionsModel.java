package poloczek.grzegorz.pl.carsquiz.data;

/**
 * Created by RENT on 2017-07-31.
 */

public class QuizQuestionsModel {

    private int id;
    private String image;
    private String answerA;
    private String answerB;
    private String answerC;
    private String answerD;
    private String correctAnswer;

    public QuizQuestionsModel(){

    }

    public QuizQuestionsModel(String image, String answerA, String answerB, String answerC, String answerD, String correctAnswer) {
        this.image = image;
        this.answerA = answerA;
        this.answerB = answerB;
        this.answerC = answerC;
        this.answerD = answerD;
        this.correctAnswer = correctAnswer;
    }

    public QuizQuestionsModel(int id, String image, String answerA, String answerB, String answerC, String answerD, String correctAnswer) {
        this.id = id;
        this.image = image;
        this.answerA = answerA;
        this.answerB = answerB;
        this.answerC = answerC;
        this.answerD = answerD;
        this.correctAnswer = correctAnswer;
    }

    public int getId() {return id;}

    public void setId(int id) {this.id = id;}

    public String getImage() {return image;}

    public void setImage(String image) {this.image = image;}

    public String getAnswerA() {return answerA;}

    public void setAnswerA(String answerA) {this.answerA = answerA;}

    public String getAnswerB() {return answerB;}

    public void setAnswerB(String answerB) {this.answerB = answerB;}

    public String getAnswerC() {return answerC;}

    public void setAnswerC(String answerC) {this.answerC = answerC;}

    public String getAnswerD() {return answerD;}

    public void setAnswerD(String answerD) {this.answerD = answerD;}

    public String getCorrectAnswer() {return correctAnswer;}

    public void setCorrectAnswer(String correctAnswer) {this.correctAnswer = correctAnswer;}

}
