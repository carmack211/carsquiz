package poloczek.grzegorz.pl.carsquiz;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;
import poloczek.grzegorz.pl.carsquiz.data.QuizDbQueries;
import poloczek.grzegorz.pl.carsquiz.data.QuizRankingModel;

/**
 * Created by Grzegorz Poloczek on 2017-08-09.
 */

public class QuizScoreboardAdapter extends RecyclerView.Adapter<QuizScoreboardAdapter.ScoreViewHolder> {

    private Context context;
    private List<QuizRankingModel> lstRanking;
    private QuizDbQueries dbQueries;

    public QuizScoreboardAdapter(Context context, List<QuizRankingModel> lstRanking) {
        this.context = context;
        this.lstRanking = lstRanking;
        dbQueries = new QuizDbQueries(context);
        dbQueries.save();
    }

    @Override
    public ScoreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.line_quiz_scoreboard,parent,false);
        return new ScoreViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ScoreViewHolder holder, int position) {
        final QuizRankingModel item = lstRanking.get(position);
        holder.removeScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbQueries.deleteToDoTask(String.valueOf(item.getId()));
                lstRanking = dbQueries.getRanking();
                notifyDataSetChanged();
            }
        });

        if(position == 0 )
            holder.trophy.setImageResource(R.drawable.gold_medal);
        else if(position == 1)
            holder.trophy.setImageResource(R.drawable.silver_medal);
        else if(position == 2)
            holder.trophy.setImageResource(R.drawable.bronze_medal);
        else
            holder.trophy.setImageResource(R.mipmap.ic_launcher_round);

        holder.name.setText(lstRanking.get(position).getName());
        holder.score.setText(String.format("%.1f",lstRanking.get(position).getScore()));
    }

    @Override
    public int getItemCount() {
        return lstRanking.size();
    }
//
//    @Override
//    public int getCount() {
//        return lstRanking.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return lstRanking.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//
//        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        convertView = inflater.inflate(R.layout.line_quiz_scoreboard, parent, false);
//
//        ImageView img = (ImageView)convertView.findViewById(R.id.quizLineImageTrophy);
//        TextView name = (TextView) convertView.findViewById(R.id.quizLineNickname);
//        TextView score = (TextView)convertView.findViewById(R.id.quizLineScore);
//
//        if(position == 0 )// top1
//            img.setImageResource(R.drawable.gold_medal);
//        else if(position == 1) // top 2
//            img.setImageResource(R.drawable.silver_medal);
//        else if(position == 2)
//            img.setImageResource(R.drawable.bronze_medal);
//        else
//            img.setImageResource(R.mipmap.ic_launcher_round);
//
//        name.setText(lstRanking.get(position).getName());
//        score.setText(String.format("%.1f",lstRanking.get(position).getScore()));
//        return convertView;
//
//    }

    public class ScoreViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView score;
        private ImageView trophy;
        private ImageView removeScore;

        public ScoreViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.quizLineNickname);
            score = (TextView) itemView.findViewById(R.id.quizLineScore);
            trophy = (ImageView) itemView.findViewById(R.id.quizLineImageTrophy);
            removeScore = (ImageView) itemView.findViewById(R.id.quizLineRemoveScore);
        }

    }
}
