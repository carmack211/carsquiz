package poloczek.grzegorz.pl.carsquiz.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by RENT on 2017-07-04.
 */

public class QuizDbQueries {

    private SQLiteDatabase database;
    private QuizDbHelper dbHelper;
    private List<QuizQuestionsModel> questionsList;



    public QuizDbQueries(Context context) {
        dbHelper = new QuizDbHelper(context);
    }

    public void save() throws SQLException {
        database =  dbHelper.getWritableDatabase();
    }

    public void load() throws SQLException {
        database = dbHelper.getReadableDatabase();
    }

    public void close() throws SQLException {
        database.close();
    }

    public void addCarToDataBase(QuizQuestionsModel quizQuestionsModel){
        load();
        ContentValues values = new ContentValues();
        values.put(QuizContract.QuizEntry.COLUMN_IMAGE, quizQuestionsModel.getImage());
        values.put(QuizContract.QuizEntry.COLUMN_ANSWERA, quizQuestionsModel.getAnswerA());
        values.put(QuizContract.QuizEntry.COLUMN_ANSWERB, quizQuestionsModel.getAnswerB());
        values.put(QuizContract.QuizEntry.COLUMN_ANSWERC, quizQuestionsModel.getAnswerC());
        values.put(QuizContract.QuizEntry.COLUMN_ANSWERD, quizQuestionsModel.getAnswerD());
        values.put(QuizContract.QuizEntry.COLUMN_CORRECT_ANSWER, quizQuestionsModel.getCorrectAnswer());
        database.insert(QuizContract.QuizEntry.TABLE_NAME, null, values);
        close();
    }

//    public List<QuizQuestionsModel> createRandomData(){
//        ArrayList<QuizQuestionsModel> quizList = new ArrayList<QuizQuestionsModel>();
//        QuizQuestionsModel quizQuestionsModel = new QuizQuestionsModel();
//        quizQuestionsModel.setImage("clio");
//        quizQuestionsModel.setAnswerA("stilo");
//        quizQuestionsModel.setAnswerB("clio");
//        quizQuestionsModel.setAnswerC("astra");
//        quizQuestionsModel.setAnswerD("twingo");
//        quizQuestionsModel.setCorrectAnswer("clio");
//        quizList.add(quizQuestionsModel);
//        addCarToDataBase(quizQuestionsModel);
//        return quizList;
//    }

    public List<QuizQuestionsModel>  initQuestions(){
        questionsList = returnQuizQuestions();
        if(questionsList.isEmpty()){
            addCarToDataBase(new QuizQuestionsModel("clio", "astra", "meriva", "jazz", "clio", "clio"));
            addCarToDataBase(new QuizQuestionsModel("mustang", "falcon", "mustang", "mondeo", "quattroporte", "mustang"));
            addCarToDataBase(new QuizQuestionsModel("stilo", "punto", "polo", "stilo", "golf", "stilo"));
            addCarToDataBase(new QuizQuestionsModel("golf", "golf", "focus", "giulietta", "megane", "golf"));
            addCarToDataBase(new QuizQuestionsModel("m3", "m1", "m2", "m3", "m5", "m3"));
            addCarToDataBase(new QuizQuestionsModel("clk", "slk", "glk", "cls", "clk", "clk"));
            addCarToDataBase(new QuizQuestionsModel("a1", "308", "focus", "clio", "a1", "a1"));
            addCarToDataBase(new QuizQuestionsModel("astra", "i30", "207", "astra", "jazz", "astra"));
            addCarToDataBase(new QuizQuestionsModel("beetle", "500", "2cv", "dmc12", "beetle", "beetle"));
            addCarToDataBase(new QuizQuestionsModel("boxster", "boxster", "4c", "elise", "lancer", "boxster"));
            addCarToDataBase(new QuizQuestionsModel("captur", "xc60", "captur", "clio", "wrangler", "captur"));
            addCarToDataBase(new QuizQuestionsModel("charger", "charger", "corvette", "viper", "mustang", "charger"));
            addCarToDataBase(new QuizQuestionsModel("colt", "t1", "jazz", "i3", "colt", "colt"));
            addCarToDataBase(new QuizQuestionsModel("corvette", "viper", "escalade", "corvette", "s2000", "corvette"));
            addCarToDataBase(new QuizQuestionsModel("dmc12", "passat", "i10", "2cv", "dmc12", "dmc12"));
            addCarToDataBase(new QuizQuestionsModel("elise", "elise", "r8", "dmc12", "z3", "elise"));
            addCarToDataBase(new QuizQuestionsModel("escalade", "t1", "x3", "wrangler", "escalade", "escalade"));
            addCarToDataBase(new QuizQuestionsModel("giulietta", "megane", "jazz", "giulietta", "agera", "giulietta"));
            addCarToDataBase(new QuizQuestionsModel("huracan", "beetle", "corvette", "viper", "huracan", "huracan"));
            addCarToDataBase(new QuizQuestionsModel("i3", "i3", "x3", "ibiza", "720s", "i3"));
            addCarToDataBase(new QuizQuestionsModel("i10", "205", "i10", "golf", "clio", "i10"));
            addCarToDataBase(new QuizQuestionsModel("i30", "jazz", "xc60", "captur", "i30", "i30"));
            addCarToDataBase(new QuizQuestionsModel("ibiza", "ibiza", "wrangler", "stilo", "lancer", "ibiza"));
            addCarToDataBase(new QuizQuestionsModel("jazz", "sandero", "giulietta", "i10", "jazz", "jazz"));
            addCarToDataBase(new QuizQuestionsModel("lancer", "m3", "lancer", "xc60", "passat", "lancer"));
            addCarToDataBase(new QuizQuestionsModel("mp4_12c", "500", "clk", "mp4_12c", "huracan", "mp4_12c"));
            addCarToDataBase(new QuizQuestionsModel("passat", "passat", "sandero", "veyron", "lancer", "passat"));
            addCarToDataBase(new QuizQuestionsModel("r8", "x3", "s2000", "r8", "720s", "r8"));
            addCarToDataBase(new QuizQuestionsModel("s2000", "viper", "911", "mr2", "s2000", "s2000"));
            addCarToDataBase(new QuizQuestionsModel("sandero", "200", "sandero", "escalade", "giulietta", "sandero"));
            addCarToDataBase(new QuizQuestionsModel("t1", "t1", "i3", "i30", "2cv", "t1"));
            addCarToDataBase(new QuizQuestionsModel("veyron", "elise", "corolla", "huracan", "veyron", "veyron"));
            addCarToDataBase(new QuizQuestionsModel("viper", "z3", "beetle", "viper", "corvette", "viper"));
            addCarToDataBase(new QuizQuestionsModel("wrangler", "wrangler", "xc60", "x3", "sandero", "wrangler"));
            addCarToDataBase(new QuizQuestionsModel("x3", "sandero", "jazz", "xc60", "x3", "x3"));
            addCarToDataBase(new QuizQuestionsModel("xc60", "i3", "4c", "astra", "xc60", "xc60"));
            addCarToDataBase(new QuizQuestionsModel("z3", "i3", "a3", "z3", "mr2", "z3"));

            questionsList = returnQuizQuestions();
        }
        return questionsList;
    }

    public void addScoreToRankTable(String name, String score){
        save();
        ContentValues values = new ContentValues();

        values.put(QuizContract.RankingEntry.COLUMN_NAME, name);
        values.put(QuizContract.RankingEntry.COLUMN_SCORE, score);

       database.insert(QuizContract.RankingEntry.TABLE_NAME, null, values);
    }

    public List<QuizQuestionsModel> returnQuizQuestions(){
        load();
        Cursor cursor = database.query(QuizContract.
                                QuizEntry.
                                TABLE_NAME, null, null, null, null, null, null);
        List<QuizQuestionsModel> list = new ArrayList<QuizQuestionsModel>();

        QuizQuestionsModel quizQuestionsModel;

        if(cursor.getCount()>0) {
            for(int i=0; i<cursor.getCount(); i++){
                cursor.moveToNext();
                quizQuestionsModel = new QuizQuestionsModel();
                quizQuestionsModel.setImage(cursor.getString(1));
                quizQuestionsModel.setAnswerA(cursor.getString(2));
                quizQuestionsModel.setAnswerB(cursor.getString(3));
                quizQuestionsModel.setAnswerC(cursor.getString(4));
                quizQuestionsModel.setAnswerD(cursor.getString(5));
                quizQuestionsModel.setCorrectAnswer(cursor.getString(6));
                list.add(quizQuestionsModel);
            }
        }
        close();
        return list;

    }

    String sortOrder = QuizContract.RankingEntry.COLUMN_SCORE + " DESC";

    String[] projection = {QuizContract.RankingEntry._ID, QuizContract.RankingEntry.COLUMN_SCORE, QuizContract.RankingEntry.COLUMN_NAME};

    public List<QuizRankingModel> getRanking() {
        List<QuizRankingModel> listRanking = new ArrayList<>();
        load();
        Cursor c;
        try {
            c = database.query(QuizContract.RankingEntry.TABLE_NAME, projection, null, null, null, null, sortOrder);
            if (c == null) return null;
            c.moveToNext();
            do {
                int id = c.getInt(c.getColumnIndex(QuizContract.RankingEntry._ID));
                double score = c.getDouble(c.getColumnIndex(QuizContract.RankingEntry.COLUMN_SCORE));
                String name = c.getString(c.getColumnIndex(QuizContract.RankingEntry.COLUMN_NAME));

                QuizRankingModel ranking = new QuizRankingModel(id, score, name);
                listRanking.add(ranking);
            } while (c.moveToNext());
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listRanking;

    }

    public void deleteToDoTask(String id){
        String selection = QuizContract.RankingEntry._ID + " = ?";
        String selectionArguments[] = { id };
        // powyzsze rowna sie temu: np.  WHERE id = ? zamienia na WHERE id = 1;
        database.delete(QuizContract.RankingEntry.TABLE_NAME, selection, selectionArguments);
    }

}
