package poloczek.grzegorz.pl.carsquiz.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;

/**
 * Created by RENT on 2017-07-31.
 */

public class QuizDbHelper extends SQLiteOpenHelper {

    //wersja bazy danych
    private static final int DATABASE_VERSION = 3;
    //nazwa bazy danych
    private static final String DATABASE_NAME = "Quiz.db";
    //ścieżka bazy danych
    private static String DATABASE_PATH = "";
    private SQLiteDatabase database;
    private Context context = null;


    public static final String SQL_CREATE_QUIZ_TABLE = "CREATE TABLE " + QuizContract.QuizEntry.TABLE_NAME
            + "(" + QuizContract.QuizEntry._ID + " INTEGER PRIMARY KEY," +
            QuizContract.QuizEntry.COLUMN_IMAGE + " TEXT," +
            QuizContract.QuizEntry.COLUMN_ANSWERA + " TEXT," +
            QuizContract.QuizEntry.COLUMN_ANSWERB + " TEXT," +
            QuizContract.QuizEntry.COLUMN_ANSWERC + " TEXT," +
            QuizContract.QuizEntry.COLUMN_ANSWERD + " TEXT," +
            QuizContract.QuizEntry.COLUMN_CORRECT_ANSWER + " TEXT)";

    public static final String SQL_CREATE_QUIZ_RANKING_TABLE = "CREATE TABLE " + QuizContract.RankingEntry.TABLE_NAME
            + "(" + QuizContract.RankingEntry._ID + " INTEGER PRIMARY KEY," +
            QuizContract.RankingEntry.COLUMN_SCORE + " TEXT," +
            QuizContract.RankingEntry.COLUMN_NAME + " TEXT)";


    private static final String SQL_DELETE_QUIZ_TABLE =
            "DROP TABLE IF EXIST " + QuizContract.QuizEntry.TABLE_NAME;

    private static final String SQL_DELETE_QUIZ_RANKING_TABLE =
            "DROP TABLE IF EXIST " + QuizContract.RankingEntry.TABLE_NAME;

    public QuizDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        DATABASE_PATH = context.getApplicationInfo().dataDir + "/databases/";
        File file = new File(DATABASE_PATH +DATABASE_NAME);
        if(file.exists())
            openDataBase();
        this.context = context;

    }

    private void openDataBase() {
        String path = DATABASE_PATH + DATABASE_NAME;
        database = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READWRITE);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_QUIZ_TABLE);
        db.execSQL(SQL_CREATE_QUIZ_RANKING_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_QUIZ_TABLE);
        db.execSQL(SQL_DELETE_QUIZ_RANKING_TABLE);
        onCreate(db);
    }
}
